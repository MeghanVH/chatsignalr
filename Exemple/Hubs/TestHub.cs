﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exemple.Hubs
{
    public class TestHub : Hub
    {
        public async Task HelloWorld(string message)
        {
            await Clients.All.SendAsync("Recevoir", $"Le serveur dit : {message}"); // équivalent du return mais il n'est pas bloquant
            //await Clients.All.SendAsync("Confirme", "Bien reçu"); 
        }

        public async Task Confirmation()
        {
            await Clients.All.SendAsync("Confirme", "I'm alive");
        }
    }
}

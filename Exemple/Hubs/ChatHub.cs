﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exemple.Hubs
{
    public class ChatHub : Hub
    {
        private static readonly ConnectionMapping<string> _connections = new ConnectionMapping<string>();
         

        //public async Task SendMessage(string user, string message)
        //{
        //    await Clients.All.SendAsync("ReceiveMessage", user, message);
        //}

        public async Task SendMessage(string user, string message)
        {
            string name = Context.User.Identity.Name;
            foreach (var connectionId in _connections.GetConnections(user))
            {
                await Clients.Client(connectionId).SendAsync(name + " : " + message);
            }
        }

        public override Task OnConnectedAsync()
        {
            // Il est toujours vide
            ////string name = Context.User.Identity.Name;
            ////_connections.Add(name, Context.ConnectionId);
            return base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            string name = Context.User.Identity.Name;
            _connections.Remove(name, Context.ConnectionId);
            return base.OnDisconnectedAsync(exception);
        }

        public void Login(string user)
        {
            string id = Context.ConnectionId;
            _connections.Add(user, id);
            

        }
    }
}
